//
// Created by lokko on 16.07.17.
//

#include <stdexcept>
#include <algorithm>
#include <iostream>
#include "Swapchain.hpp"
#include "ImageViews.hpp"//TODO merge into swapchainmanager

VkSurfaceFormatKHR chooseSwapSurfaceFormat(const VkSurfaceKHR& surface,
                                           const VkPhysicalDevice& physicalDevice,
                                           const VkSurfaceFormatKHR& requestedFormat)
{
    uint32_t formatCount = 0;
    std::vector<VkSurfaceFormatKHR> availableFormats;
    if (vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, nullptr) != VK_SUCCESS)
        std::runtime_error("couldn't get surface formats");
    availableFormats.resize(formatCount);
    vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, availableFormats.data());
    if (formatCount == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED)
        return requestedFormat;
    for (const auto& availableFormat : availableFormats)
        if (availableFormat.colorSpace == requestedFormat.colorSpace && availableFormat.format == requestedFormat.format)
            return requestedFormat;
    return availableFormats[0];
}

VkPresentModeKHR chooseSwapPresentMode(const VkSurfaceKHR& surface,
                                       const VkPhysicalDevice& physicalDevice,
                                       const VkPresentModeKHR& requestedMode)
{
    uint32_t presentModeCount = 0;
    std::vector<VkPresentModeKHR> availablePresentModes;
    if (vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, nullptr) != VK_SUCCESS)
        std::runtime_error("couldn't get surface present modes");
    availablePresentModes.resize(presentModeCount);
    vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModeCount, availablePresentModes.data());
    for (const auto& availablePresentMode : availablePresentModes)
        if (availablePresentMode == requestedMode)
            return requestedMode;
    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities,
                            const VkExtent2D &extent)
{
    if (capabilities.currentExtent.width != UINT32_MAX)
        return capabilities.currentExtent;
    else
        return {std::clamp(extent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
                std::clamp(extent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height)};
}

SwapchainRequirements::SwapchainRequirements(Device& device, const VkSurfaceKHR& surfaceIn)
        : VkSwapchainCreateInfoKHR(),
          device{device}
{
    sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    surface = surfaceIn;
};

Swapchain::SurfaceCapabilities::SurfaceCapabilities(const VkSurfaceKHR& surface,
                                                    const VkPhysicalDevice& physicalDevice)
        : VkSurfaceCapabilitiesKHR()
{
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, this);
}

Swapchain::Swapchain(const SwapchainRequirements& requestedSwapchain)
        : SwapchainRequirements(requestedSwapchain.device, requestedSwapchain.surface)
{
    VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(requestedSwapchain.surface, requestedSwapchain.device.physicalDevice, {requestedSwapchain.imageFormat, requestedSwapchain.imageColorSpace});
    SurfaceCapabilities capabilities(requestedSwapchain.surface, device.physicalDevice);

    presentMode = chooseSwapPresentMode(requestedSwapchain.surface, device.physicalDevice, requestedSwapchain.presentMode);
    imageExtent = chooseSwapExtent(capabilities, requestedSwapchain.imageExtent);
    minImageCount = std::clamp(requestedSwapchain.minImageCount, capabilities.minImageCount, capabilities.maxImageCount);
    surface = requestedSwapchain.surface;
    imageFormat = surfaceFormat.format;
    imageColorSpace = surfaceFormat.colorSpace;
    imageArrayLayers = 1;
    imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    queueFamilyIndexCount = static_cast<uint32_t>(requestedSwapchain.graphicsQueueFamilyIndices.size());
    graphicsQueueFamilyIndices = requestedSwapchain.graphicsQueueFamilyIndices;
    pQueueFamilyIndices = graphicsQueueFamilyIndices.data();
    imageSharingMode = requestedSwapchain.graphicsQueueFamilyIndices.size() > 1 ? VK_SHARING_MODE_CONCURRENT
                                                                                : VK_SHARING_MODE_EXCLUSIVE;
    preTransform = capabilities.currentTransform;
    compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    clipped = VK_TRUE;
    oldSwapchain = VK_NULL_HANDLE;

    if (vkCreateSwapchainKHR(device.handle, this, nullptr, &handle) != VK_SUCCESS)
        throw std::runtime_error("failed to create swap chain!");

    vkGetSwapchainImagesKHR(device.handle, handle, &minImageCount, nullptr);
    swapchainImages.resize(minImageCount);
    vkGetSwapchainImagesKHR(device.handle, handle, &minImageCount, swapchainImages.data());
    imageViews = ImageViews(*this);
}

Swapchain::~Swapchain()
{
    for (auto imageView : imageViews)
        vkDestroyImageView(device.handle, imageView, nullptr);
    vkDestroySwapchainKHR(device.handle, handle, nullptr);
}
