//
// Created by lokko on 18.09.17.
//

#ifndef ENGINE_IMAGEVIEWMANAGER_HPP
#define ENGINE_IMAGEVIEWMANAGER_HPP

#include "glfw3.hpp"
#include <vector>
#include "Swapchain.hpp"

struct ImageViews : std::vector<VkImageView>
{
    const VkDevice& device;
    ImageViews() = default;
    ImageViews(const Swapchain& swapchain);
    ~ImageViews();
private:
    struct ImageView : VkImageViewCreateInfo
    {
        ImageView(VkImageView& swapChainImageView,
                  const VkImage& swapchainImage,
                  const Swapchain& swapchain);
    };
};



#endif //ENGINE_IMAGEVIEWMANAGER_HPP
