//
// Created by lokko on 7.07.17.
//

#ifndef ENGINE_WINDOWMANAGER_HPP
#define ENGINE_WINDOWMANAGER_HPP

#include <functional>
#include "RenderLoop.hpp"
#include "../LinearAlgebra/Vectors.hpp"

class Window
{
    std::vector<std::function<void()>> mouseClickCallbacks;
    static void characterCallback(GLFWwindow *window, unsigned int keycode);
    static void cursorPosCallback(GLFWwindow *window, double xpos, double ypos);
    static void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods);
    static void scrollCallback(GLFWwindow *window, double xoffset, double yoffset);
    static void dropCallback(GLFWwindow *window, int count, const char** paths);
    static void cursorEnterCallback(GLFWwindow *window, int entered);
    VkSurfaceKHR _surface;
    GLFWwindow* _handle;
    const VkInstance& instance;
public:
    void setCharacterCallback();
    void removeCharacterCallback();
    void setCursorPosCallback();
    void removeCursorPosCallback();
    void setMouseButtonCallback();
    void removeMouseButtonCallback();
    void setScrollCallback();
    void removeScrollCallback();
    void setDropCallback();
    void removeDropCallback();
    void setCursorEnterCallback();
    void removeCursorEnterCallback();
    void addMouseClickCallback(std::function<void()>);

    VkSurfaceKHR const& surface;
    GLFWwindow* const& handle;
    RenderLoopManager renderLoopManager;

    struct {
        vec2t<int> pos;
        int button;
        int action;
        int mods;
    }MouseClick;

    Window(VkInstance instance, uint32_t, uint32_t, std::unique_ptr<RenderLoop>(&)(Device&, const VkSurfaceKHR&), Device&);
    ~Window();
};

#endif //ENGINE_WINDOWMANAGER_HPP
