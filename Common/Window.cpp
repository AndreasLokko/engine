//
// Created by lokko on 7.07.17.
//

#include "Window.hpp"
#include <iostream>

void onWindowResized(GLFWwindow* window, int width, int height)
{
    static_cast<Window*>(glfwGetWindowUserPointer(window))->renderLoopManager.UpdateResolution(width, height);
}

Window::Window(VkInstance instance, uint32_t width, uint32_t height, std::unique_ptr<RenderLoop>(&factory)(Device&, const VkSurfaceKHR&), Device& device)
        : handle{[&]()->GLFWwindow*&
                 {
                     glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
                     glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
                     glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
                     _handle = glfwCreateWindow(width, height, "Vulkan", nullptr, nullptr);
                     if (glfwCreateWindowSurface(instance, _handle, nullptr, &_surface) != VK_SUCCESS)
                         throw std::runtime_error("failed to create window surface!");
                     glfwSetWindowUserPointer(_handle, this);
                     return _handle;
                 }()},
          surface{_surface},
          instance{instance},
          renderLoopManager{_handle, factory, device, surface}
{
    glfwSetWindowSizeCallback(handle, onWindowResized);
}

void Window::setCharacterCallback()
{
    glfwSetCharCallback(_handle, characterCallback);
}

void Window::characterCallback(GLFWwindow *window, unsigned int keycode)
{
    std::cout<< char(keycode) << std::endl;
}

void Window::removeCharacterCallback()
{
    glfwSetCharCallback(_handle, nullptr);
}

void Window::setCursorPosCallback() {
    glfwSetCursorPosCallback(_handle, cursorPosCallback);
}

void Window::cursorPosCallback(GLFWwindow *window, double _xpos, double _ypos){
    std::cout<< _xpos <<" "<< _ypos << std::endl;
}

void Window::removeCursorPosCallback() {
    glfwSetCursorPosCallback(_handle, nullptr);
}

void Window::setMouseButtonCallback() {
    glfwSetMouseButtonCallback(_handle, mouseButtonCallback);
}

void Window::mouseButtonCallback(GLFWwindow *window, int button, int action, int mods) {
    if(action & (GLFW_PRESS | GLFW_REPEAT | GLFW_RELEASE)) {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        Window* m = static_cast<Window*>(glfwGetWindowUserPointer(window));
        m->MouseClick.pos = vec2t<int>(xpos, ypos);
        m->MouseClick.button = button;
        m->MouseClick.action = action;
        for(auto callback : m->mouseClickCallbacks)
            callback();
    }
}

void Window::removeMouseButtonCallback() {
    glfwSetMouseButtonCallback(_handle, nullptr);
}

void Window::setScrollCallback() {
    glfwSetScrollCallback(_handle, scrollCallback);
}

void Window::scrollCallback(GLFWwindow *window, double xoffset, double yoffset) {
    std::cout<< xoffset << ":" << yoffset <<std::endl;
}

void Window::removeScrollCallback() {
    glfwSetScrollCallback(_handle, nullptr);
}

void Window::setDropCallback() {
    glfwSetDropCallback(_handle, dropCallback);
}

void Window::dropCallback(GLFWwindow *window, int count, const char **paths) {
    while(count-- > 0)
        std::cout<< paths[count] <<std::endl;
}

void Window::removeDropCallback() {
    glfwSetDropCallback(_handle, nullptr);
}

void Window::setCursorEnterCallback() {
    glfwSetCursorEnterCallback(_handle, cursorEnterCallback);
}

void Window::cursorEnterCallback(GLFWwindow *window, int entered) {

}

void Window::removeCursorEnterCallback() {
    glfwSetCursorEnterCallback(_handle, nullptr);
}

void Window::addMouseClickCallback(std::function<void()> callback) {
    mouseClickCallbacks.push_back(callback);
}

Window::~Window() {
    vkDestroySurfaceKHR(instance, surface, nullptr);
    glfwDestroyWindow(handle);
}