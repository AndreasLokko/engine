//
// Created by lokko on 5.12.17.
//

#ifndef ENGINE_FRAMEBUFFERS_HPP
#define ENGINE_FRAMEBUFFERS_HPP

#include <vector>
#include "RenderPass.hpp"

struct FrameBuffers : std::vector<VkFramebuffer>
{
    const VkDevice& device;
    FrameBuffers(const RenderPass& renderPass);
    ~FrameBuffers();
};


#endif //ENGINE_FRAMEBUFFERS_HPP
