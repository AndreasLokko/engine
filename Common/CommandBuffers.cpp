//
// Created by lokko on 14.12.17.
//

#include "CommandBuffers.hpp"

CommandBuffers::CommandBuffers(const SwapchainRequirements& requirements)
        : graphicsPipeline{requirements}
{
    this->resize(graphicsPipeline.frameBuffers.size());
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = requirements.device.commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = static_cast<uint32_t>(this->size());
    if (vkAllocateCommandBuffers(requirements.device.handle, &allocInfo, this->data()) != VK_SUCCESS)
        throw std::runtime_error("failed to allocate command buffers!");

    auto frameBufferIterator = graphicsPipeline.frameBuffers.begin();
    for (auto& commandBuffer : *this) {
        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        vkBeginCommandBuffer(commandBuffer, &beginInfo);
        VkRenderPassBeginInfo renderPassInfo = {};
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = graphicsPipeline.renderPass.handle;
        renderPassInfo.framebuffer = *frameBufferIterator++;
        renderPassInfo.renderArea.offset = {0, 0};
        renderPassInfo.renderArea.extent = graphicsPipeline.renderPass.swapchain.imageExtent;
        VkClearValue clearColor = {0.0f, 0.0f, 0.0f, 1.0f};//TODO this value dies but the reference goes on
        renderPassInfo.clearValueCount = 1;
        renderPassInfo.pClearValues = &clearColor;
        vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline.handle);
        vkCmdDraw(commandBuffer, 3, 1, 0, 0);
        vkCmdEndRenderPass(commandBuffer);
        if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
            throw std::runtime_error("failed to record command buffer!");
    }
}

CommandBuffers::~CommandBuffers()
{
    vkFreeCommandBuffers(graphicsPipeline.renderPass.swapchain.device.handle,
                         graphicsPipeline.renderPass.swapchain.device.commandPool,
                         static_cast<uint32_t>(size()),
                         data());
}
