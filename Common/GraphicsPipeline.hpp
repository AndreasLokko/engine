//
// Created by lokko on 18.09.17.
//

#ifndef ENGINE_GRAPHICSPIPELINEMANAGER_HPP
#define ENGINE_GRAPHICSPIPELINEMANAGER_HPP

#include "RenderPass.hpp"
#include "FrameBuffers.hpp"

class GraphicsPipeline
{
    std::vector<char> readFile(const std::string& filename);
    VkShaderModule createShaderModule(const std::vector<char>& code,
                                      const VkDevice& device);
    VkPipelineLayout pipelineLayout;
public:
    VkPipeline handle;
    const RenderPass renderPass;
    const FrameBuffers frameBuffers;
    GraphicsPipeline(const SwapchainRequirements&);
    ~GraphicsPipeline();
};


#endif //ENGINE_GRAPHICSPIPELINEMANAGER_HPP
