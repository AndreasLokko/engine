//
// Created by lokko on 10.12.17.
//

#ifndef ENGINE_COMMANDPOOL_HPP
#define ENGINE_COMMANDPOOL_HPP

#include "glfw3.hpp"
#include <stdexcept>

struct CommandPool {
    VkCommandPool handle;
    CommandPool(const VkDevice& device)
    {
        VkCommandPoolCreateInfo poolInfo = {};
        poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        poolInfo.queueFamilyIndex = 0;//TODO
        if (vkCreateCommandPool(device, &poolInfo, nullptr, &handle) != VK_SUCCESS)
            throw std::runtime_error("failed to create command pool!");
    }
};


#endif //ENGINE_COMMANDPOOL_HPP
