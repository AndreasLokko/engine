//
// Created by lokko on 16.07.17.
//

#ifndef ENGINE_SWAPCHAIN_HPP
#define ENGINE_SWAPCHAIN_HPP

#include <vector>
#include <Device.hpp>

struct SwapchainRequirements : VkSwapchainCreateInfoKHR
{
    std::vector<uint32_t> graphicsQueueFamilyIndices;
    Device& device;
    //const RenderLoop renderLoop;
    SwapchainRequirements(Device&, const VkSurfaceKHR&);
};

struct Swapchain : SwapchainRequirements
{
    Swapchain(const SwapchainRequirements&);
    ~Swapchain();
    std::vector<VkImageView> imageViews;
    VkSwapchainKHR handle;
    std::vector<VkImage> swapchainImages;
    struct SurfaceCapabilities
            : VkSurfaceCapabilitiesKHR
    {
        SurfaceCapabilities(const VkSurfaceKHR&,
                            const VkPhysicalDevice&);
    };
};

#endif //ENGINE_SWAPCHAIN_HPP