//
// Created by lokko on 11.01.18.
//

#ifndef ENGINE_RENDERLOOP_HPP
#define ENGINE_RENDERLOOP_HPP

#include <thread>
#include <mutex>
#include "CommandBuffers.hpp"

class RenderLoop
{
    virtual bool drawFrame(CommandBuffers& commandBuffers) = 0;
    friend void runRenderLoop(GLFWwindow *const&, std::unique_ptr<RenderLoop>&&, std::atomic_flag&);
public:
    RenderLoop(Device& device, const VkSurfaceKHR& surface) : swapchainRequirements{device, surface}{}
    virtual ~RenderLoop() = 0;
    SwapchainRequirements swapchainRequirements;
};

class RenderLoopManager
{
    std::unique_ptr<RenderLoop>(*currentFactory)(Device&, const VkSurfaceKHR&);
    std::unique_ptr<RenderLoop>(*requestedFactory)(Device&, const VkSurfaceKHR&);
    Device* requestedDevice;
    VkSurfaceKHR requestedSurface;
    std::atomic_flag isSwapchainNotDirty{true};
    std::thread thread;
    std::mutex mutex;
public:
    RenderLoopManager(GLFWwindow *const &window, std::unique_ptr<RenderLoop>(&)(Device&, const VkSurfaceKHR&), Device&, const VkSurfaceKHR&);
    void UpdateRenderLoop(std::unique_ptr<RenderLoop>(&)(Device&, const VkSurfaceKHR&), SwapchainRequirements swapchainRequirements);
    void UpdateResolution(int width, int Height);
    void wait();
};

#endif //ENGINE_RENDERLOOP_HPP
