//
// Created by lokko on 5.12.17.
//

#include "FrameBuffers.hpp"

void createFramebuffer(VkFramebuffer& framebuffer,
                       const VkImageView& imageView,
                       const RenderPass& renderPass);

FrameBuffers::FrameBuffers(const RenderPass& renderPass)
        : device(renderPass.swapchain.device.handle)
{
    resize(renderPass.swapchain.imageViews.size());
    auto imageViewIterator(renderPass.swapchain.imageViews.begin());
    for (auto &framebuffer : *this)
        createFramebuffer(framebuffer, *imageViewIterator++, renderPass);
}

FrameBuffers::~FrameBuffers()
{
    for (auto framebuffer : *this)
        vkDestroyFramebuffer(device, framebuffer, nullptr);
}

void createFramebuffer(VkFramebuffer& framebuffer,
                       const VkImageView& imageView,
                       const RenderPass& renderPass)
{
    std::vector<VkImageView> attachments = { imageView };
    VkFramebufferCreateInfo framebufferInfo = {};
    framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferInfo.renderPass = renderPass.handle;
    framebufferInfo.attachmentCount = 1;
    framebufferInfo.pAttachments = attachments.data();
    framebufferInfo.width = renderPass.swapchain.imageExtent.width;
    framebufferInfo.height = renderPass.swapchain.imageExtent.height;
    framebufferInfo.layers = 1;
    if (vkCreateFramebuffer(renderPass.swapchain.device.handle, &framebufferInfo, nullptr, &framebuffer) != VK_SUCCESS)
        throw std::runtime_error("failed to create framebuffer!");
}
