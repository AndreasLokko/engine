//
// Created by lokko on 19.09.17.
//

#ifndef ENGINE_RENDERPASSMANAGER_HPP
#define ENGINE_RENDERPASSMANAGER_HPP

#include "Swapchain.hpp"

struct RenderPass
{
    VkRenderPass handle;
    const Swapchain swapchain;
    RenderPass(const SwapchainRequirements& requirements);
    ~RenderPass();
};


#endif //ENGINE_RENDERPASSMANAGER_HPP