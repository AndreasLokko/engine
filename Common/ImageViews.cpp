//
// Created by lokko on 18.09.17.
//

#include <stdexcept>
#include "ImageViews.hpp"
#include "Swapchain.hpp"

ImageViews::ImageViews(const Swapchain& swapchain)
        : device{swapchain.device.handle}
{
    resize(swapchain.swapchainImages.size());
    auto swapChainImagesIterator = swapchain.swapchainImages.begin();
    for (auto& swapChainImageView : *this)
        ImageView(swapChainImageView, *swapChainImagesIterator++, swapchain);
}

ImageViews::~ImageViews()
{
    for (auto imageView : *this)
        vkDestroyImageView(device, imageView, nullptr);
}

ImageViews::ImageView::ImageView(VkImageView& swapChainImageView,
                                 const VkImage &swapchainImage,
                                 const Swapchain &swapchain)
: VkImageViewCreateInfo()
{
    sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image = swapchainImage;
    viewType = VK_IMAGE_VIEW_TYPE_2D;
    format = swapchain.imageFormat;
    components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresourceRange.baseMipLevel = 0;
    subresourceRange.levelCount = 1;
    subresourceRange.baseArrayLayer = 0;
    subresourceRange.layerCount = 1;

    if (vkCreateImageView(swapchain.device.handle, this, nullptr, &swapChainImageView) != VK_SUCCESS)
        throw std::runtime_error("failed to create image views!");
}