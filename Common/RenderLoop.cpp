//
// Created by lokko on 11.01.18.
//

#include <chrono>
#include <iostream>
#include "RenderLoop.hpp"

void runRenderLoop(GLFWwindow *const &window,
                   std::unique_ptr<RenderLoop>&& renderLoop,
                   std::atomic_flag& isSwapchainNotDirty)
{
    CommandBuffers commandBuffers(renderLoop->swapchainRequirements);
    while (glfwWindowShouldClose(window) == VK_FALSE
           && renderLoop->drawFrame(commandBuffers)
           && (isSwapchainNotDirty.test_and_set()));
    vkDeviceWaitIdle(commandBuffers.graphicsPipeline.renderPass.swapchain.device.handle);
};

void run(GLFWwindow *const &window,
         std::unique_ptr<RenderLoop>(&factory)(Device&, const VkSurfaceKHR&),
         Device& device,
         const VkSurfaceKHR& surface,
         std::atomic_flag& isSwapchainNotDirty)
{
    while (glfwWindowShouldClose(window) == VK_FALSE)
        runRenderLoop(window, factory(device, surface), isSwapchainNotDirty);
    std::cout<< "cleaning" <<std::endl;
}

RenderLoopManager::RenderLoopManager(GLFWwindow *const &window, // TODO get rid of this and replace with generic atomic bool or predicate
                                     std::unique_ptr<RenderLoop>(&factory)(Device&, const VkSurfaceKHR&),
                                     Device& device,
                                     const VkSurfaceKHR& surface)
        :currentFactory{factory},
         thread{run, window, std::ref(factory), std::ref(device), std::ref(surface), std::ref(isSwapchainNotDirty)}{}

void RenderLoopManager::UpdateRenderLoop(std::unique_ptr<RenderLoop>(&factory)(Device&, const VkSurfaceKHR&),
                                         SwapchainRequirements swapchainRequirements)
{
    std::scoped_lock lock{mutex};
    requestedFactory = factory;
    //new(&renderLoop->swapchainRequirements) SwapchainRequirements{swapchainRequirements};
    isSwapchainNotDirty.clear();
}

void RenderLoopManager::UpdateResolution(int width, int height)
{
    std::scoped_lock lock{mutex};
    //renderLoop->swapchainRequirements.imageExtent = {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
    isSwapchainNotDirty.clear();
}

bool isSwapchainNotDirty(const Swapchain& swapchain, const RenderLoop& renderLoop, std::mutex& mutex)
{
    std::scoped_lock lock{mutex};
    return (swapchain.imageExtent.width == 0)
            || (swapchain.imageExtent.height == 0)
            || ((swapchain.surface == renderLoop.swapchainRequirements.surface)
            && (swapchain.imageExtent.width == renderLoop.swapchainRequirements.imageExtent.width)
            && (swapchain.imageExtent.height == renderLoop.swapchainRequirements.imageExtent.height)
            && (swapchain.imageFormat == renderLoop.swapchainRequirements.imageFormat)
            && (swapchain.minImageCount >= renderLoop.swapchainRequirements.minImageCount)
            && (swapchain.imageColorSpace == renderLoop.swapchainRequirements.imageColorSpace)
//           && (swapchain.imageArrayLayers == swapchainRequirements.imageArrayLayers)
            && (swapchain.flags == renderLoop.swapchainRequirements.flags)
            && (swapchain.presentMode == renderLoop.swapchainRequirements.presentMode
            || (swapchain.presentMode == VK_PRESENT_MODE_FIFO_KHR))
            && (swapchain.graphicsQueueFamilyIndices.size() == renderLoop.swapchainRequirements.graphicsQueueFamilyIndices.size())
            && [&]()-> bool {
                                int i = 0;
                                for(auto index : swapchain.graphicsQueueFamilyIndices)
                                    if(index != renderLoop.swapchainRequirements.graphicsQueueFamilyIndices[i++])
                                        return false;
                                return true;
                            }()
//           && (swapchain.clipped == swapchainRequirements.clipped)
//           && (swapchain.compositeAlpha == swapchainRequirements.compositeAlpha)
//           && (swapchain.imageSharingMode == swapchainRequirements.imageSharingMode)
//           && (swapchain.imageUsage == swapchainRequirements.imageUsage)
//           && (swapchain.preTransform == swapchainRequirements.preTransform)
            );
}

void RenderLoopManager::wait()
{
    thread.join();
}

RenderLoop::~RenderLoop() {}
