//
// Created by lokko on 14.12.17.
//

#ifndef ENGINE_COMMANDBUFFER_HPP
#define ENGINE_COMMANDBUFFER_HPP

#include <vector>
#include "GraphicsPipeline.hpp"

struct CommandBuffers : private std::vector<VkCommandBuffer>
{
    using vector::operator[];
    const GraphicsPipeline graphicsPipeline;
    CommandBuffers(const SwapchainRequirements&);
    ~CommandBuffers();
};


#endif //ENGINE_COMMANDBUFFER_HPP
