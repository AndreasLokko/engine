//
// Created by lokko on 5.07.17.
//

#ifndef ENGINE_APPLICATION_HPP
#define ENGINE_APPLICATION_HPP

#include "glfw3.hpp"

void Application(const VkInstance& instance);

#endif //ENGINE_APPLICATION_HPP
