//
// Created by lokko on 13.08.17.
//

#ifndef ENGINE_GUIELEMENTS_HPP
#define ENGINE_GUIELEMENTS_HPP


class GUI {
    class Button{
        void OnPress();
    };

    class Toggle{
        bool value;
    };

    template<typename T = float>
    class Slider{
        T min, max, value;
    };

    class Textbox{

    };

    class Dropdown{

    };

    class Image{

    };

    class Label{

    };

    class Console{

    };

    class Log{

    };
};


#endif //ENGINE_GUIELEMENTS_HPP
