//
// Created by lokko on 5.07.17.
//

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include "EditorLayout.hpp"
#include "../Common/Window.hpp"

//TODO clean this up. move to stack.
const vec2t<int*> EditorLayout::UL(new int(0), new int(0));
const vec2t<int*> EditorLayout::LR(new int(800), new int(450));
const EditorLayout::PaneNode EditorLayout::editorRootWindow(nullptr, EditorLayout::PaneCoordinates(UL, LR));

EditorLayout::PaneNode::~PaneNode() {
    delete this->children->x;
    delete this->children->y;
    delete children;
}

EditorLayout::PaneNode::PaneNode(PaneNode* _parentPane, const EditorLayout::PaneCoordinates& _vertices)
        : parentWindow(_parentPane), children(nullptr), vertices(_vertices) , minSpan(0, 0) {}

template<>
void EditorLayout::PaneNode::addSplitPane<top>(EditorLayout::PaneNode *window, vec2t<int> split) {
    children = new vec2t<PaneNode*>(new PaneNode(this, vertices), new PaneNode(this, vertices));
    childrenSplitCoordinate = split.y;
    children->x->vertices.LR.y = &childrenSplitCoordinate;
    children->y->vertices.UL.y = &childrenSplitCoordinate;
    isDivideVertical = false;
}

template<>
void EditorLayout::PaneNode::addSplitPane<bottom>(EditorLayout::PaneNode *window, vec2t<int> split) {
    children = new vec2t<PaneNode*>(new PaneNode(this, vertices), new PaneNode(this, vertices));
    childrenSplitCoordinate = split.y;
    children->x->vertices.LR.y = &childrenSplitCoordinate;
    children->y->vertices.UL.y = &childrenSplitCoordinate;
    isDivideVertical = false;
}

template<>
void EditorLayout::PaneNode::addSplitPane<left>(EditorLayout::PaneNode *window, vec2t<int> split) {
    children = new vec2t<PaneNode*>(new PaneNode(this, vertices), new PaneNode(this, vertices));
    childrenSplitCoordinate = split.x;
    children->x->vertices.LR.x = &childrenSplitCoordinate;
    children->y->vertices.UL.x = &childrenSplitCoordinate;
    isDivideVertical = true;
}

template<>
void EditorLayout::PaneNode::addSplitPane<right>(EditorLayout::PaneNode *window, vec2t<int> split) {
    children = new vec2t<PaneNode*>(new PaneNode(this, vertices), new PaneNode(this, vertices));
    childrenSplitCoordinate = split.x;
    children->x->vertices.LR.x = &childrenSplitCoordinate;
    children->y->vertices.UL.x = &childrenSplitCoordinate;
    isDivideVertical = true;
}

void EditorLayout::PaneNode::MouseInput(vec2t<int> mousePos)const {
    if (children == nullptr) MouseCallbacks();
    else if(children->x->vertices.Enclose(mousePos)) children->x->MouseInput(mousePos);
    else children->x->MouseInput(mousePos);
}

void EditorLayout::PaneNode::MouseCallbacks()const {
    std::cout<<"ananass"<<std::endl;
}

bool EditorLayout::PaneCoordinates::Enclose(vec2t<int> pos) {
    return *UL.x <= pos.x &&
           *UL.y <= pos.y &&
           *LR.x > pos.x &&
           *LR.y > pos.y;
}

void EditorLayout::PaneNode::saveLayout(std::string layoutName)const {

    struct stat info;
#ifdef _WIN32
    if( stat( "Layouts", &info ) != 0 ) mkdir("Layouts");
        else if(!( info.st_mode & S_IFDIR )) mkdir("Layouts");
#else
    if( stat( "Layouts", &info ) != 0 ) mkdir("Layouts", S_IRWXU );
    else if(!( info.st_mode & S_IFDIR )) mkdir("Layouts", S_IRWXU );
#endif
    std::fstream file("Layouts/" + layoutName + ".penis", file.binary | file.trunc | file.out);
    if (file.is_open()) {
        std::vector<PaneNode*> stack;
        stack.push_back((PaneNode*)this);
        int p = 0;
        int c = 0;
        for (PaneNode* node : stack) {
            file << p++ << std::endl;
            file << node->minSpan.x << std::endl;
            file << node->minSpan.y << std::endl;
            file << *(node->vertices.UL.x) << std::endl;
            file << *(node->vertices.UL.y) << std::endl;
            file << *(node->vertices.LR.x) << std::endl;
            file << *(node->vertices.LR.y) << std::endl;
            if (node->children == nullptr) {
                file << 0 << std::endl;
                file << 0 << std::endl;
            }
            else {
                stack.push_back(node->children->x);
                stack.push_back(node->children->y);
                file << ++c << std::endl;
                file << ++c << std::endl;
            }
            file << node->isDivideVertical << std::endl;
        }
        file.close();
    }
    else throw std::runtime_error("layout not saved");
}
