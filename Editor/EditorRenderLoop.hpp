//
// Created by lokko on 14.02.18.
//

#ifndef ENGINE_EDITORRENDERLOOP_HPP
#define ENGINE_EDITORRENDERLOOP_HPP

#include "../Common/RenderLoop.hpp"

class EditorRenderLoop : public RenderLoop
{
    VkSemaphore imageAvailableSemaphore, renderFinishedSemaphore;
    VkQueue graphicsQueue, presentQueue;
public:
    bool drawFrame(CommandBuffers& commandBuffers) override;
    EditorRenderLoop(Device& device, const VkSurfaceKHR&);
    ~EditorRenderLoop();
    static std::unique_ptr<RenderLoop> factory(Device& device, const VkSurfaceKHR&);
};


#endif //ENGINE_EDITORRENDERLOOP_HPP
