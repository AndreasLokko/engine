//
// Created by lokko on 8.11.17.
//

#ifndef ENGINE_STRINGEXTRACTOR_HPP
#define ENGINE_STRINGEXTRACTOR_HPP


#include <type_traits>
#include <istream>

std::string s;
    class scalar{
        friend std::istream &operator>>(std::istream &is, scalar& self) {
            long long temp;
            is >> temp;
            return  is;
        };
    };

    class string : std::string {
        friend std::istream &operator>>(std::istream &is, string& self) {
            return  is;
        };
    };

    class boolean{
        friend std::istream &operator>>(std::istream &is, boolean& self) {
            return  is;
        };
    };

    class structural{
        friend std::istream &operator>>(std::istream &is, structural& self) {
            return  is;
        };
    };

    template<typename T>
    T operator>>(std::string s, T& var){
        if constexpr(std::is_object<T>::value) {
            if constexpr(std::is_scalar<T>::value) {
                if (std::is_floating_point<T>::value || s.find_first_of(".pP") != s.npos ||
                    (s.front() != '0' && s.find_first_of("eE") != s.npos)) {
                    long double tmp = std::stold(s);
                    if (std::numeric_limits<T>::lowest() > tmp && tmp > std::numeric_limits<T>::max())
                        throw std::invalid_argument("argument out of range");
                    else return tmp;
                } else {
                    std::conditional<(std::is_signed<T>::value), long long, unsigned long long> tmp;
                        if(std::is_signed<T>::value)
                            tmp = std::stol(s);
                        else
                            tmp = std::stoul(s);
                    if (std::numeric_limits<T>::lowest() > tmp && tmp > std::numeric_limits<T>::max())
                        throw std::invalid_argument("argument out of range");
                    else return tmp;
                }
            } else if constexpr (std::is_array<T>::value) {
                structural temp;
                s >> temp;
                var = temp;
            } else if constexpr (std::is_array<T>::value) {
                structural temp;
                s >> temp;
                var = temp;
            } else if constexpr (std::is_array<T>::value) {
                structural temp;
                s >> temp;
                var = temp;
            } else {
                structural temp;
                s >> temp;
                var = temp;
            }
        } else throw std::invalid_argument("unsupported argument type");
    };



#endif //ENGINE_STRINGEXTRACTOR_HPP
