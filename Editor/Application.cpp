//
// Created by lokko on 5.07.17.
//

#include <iostream>
#include "Application.hpp"
#include "EditorLayout.hpp"
#include "../Common/Window.hpp"
#include "../CommandConsole/ConsoleCommands.hpp"
#include "EditorRenderLoop.hpp"

void Application(const VkInstance& instance)
{
    PhysicalDevice physicalDevice = reserveDevice();
    if(physicalDevice.handle == VK_NULL_HANDLE)
        throw std::runtime_error("Couldn't secure a GPU");

    Device device{physicalDevice};
    Window mainWindow{instance, 800, 450, EditorRenderLoop::factory, device};
        mainWindow.setCharacterCallback();
        mainWindow.setMouseButtonCallback();
        mainWindow.setScrollCallback();
        mainWindow.setDropCallback();
        mainWindow.addMouseClickCallback([&mainWindow]{EditorLayout::editorRootWindow.MouseInput(mainWindow.MouseClick.pos);});

    //PhysicsLoop
    //LogicLoop
    //Networking
    //Audio
    mainWindow.renderLoopManager.wait();
    std::cout<< "GAME OVER!" <<std::endl;
}