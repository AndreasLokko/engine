//
// Created by lokko on 5.07.17.
//

#ifndef ENGINE_EDITORLAYOUTMANAGER_HPP
#define ENGINE_EDITORLAYOUTMANAGER_HPP

#include <vector>
#include "../LinearAlgebra/Vectors.hpp"

enum Position{
    top,
    bottom,
    left,
    right
};

class EditorLayout {

    static const vec2t<int*> UL;
    static const vec2t<int*> LR;

    struct PaneCoordinates
    {
        vec2t<int*> UL;
        vec2t<int*> LR;

        PaneCoordinates(const vec2t<int*>& _UL, const vec2t<int*>& _LR): UL(_UL), LR(_LR) {}
        bool Enclose(vec2t<int> pos);
    };

    class PaneNode
    {
        PaneNode* parentWindow;
        vec2t<PaneNode*>* children;
        PaneCoordinates vertices;
        int childrenSplitCoordinate;
        vec2t<int> minSpan;
        bool isDivideVertical;

        template<Position pos>
        void addSplitPane(PaneNode *window, vec2t<int> split);
        void MouseCallbacks()const;

    public:
        PaneNode(PaneNode* _parentPane, const PaneCoordinates& _vertices);
        void MouseInput(vec2t<int> mousePos)const;
        void saveLayout(std::string layoutName)const;
        ~PaneNode();
    };

public:
    static const PaneNode editorRootWindow;
};



#endif //ENGINE_EDITORLAYOUTMANAGER_HPP
