//
// Created by lokko on 14.02.18.
//

#include <iostream>
#include "EditorRenderLoop.hpp"

bool EditorRenderLoop::drawFrame(CommandBuffers& commandBuffers)
{
    auto start = std::chrono::system_clock::now();
    uint32_t imageIndex;
    VkResult result = vkAcquireNextImageKHR(
            commandBuffers.graphicsPipeline.renderPass.swapchain.device.handle,
            commandBuffers.graphicsPipeline.renderPass.swapchain.handle,
            std::numeric_limits<uint64_t>::max(),
            imageAvailableSemaphore,
            VK_NULL_HANDLE,
            &imageIndex);
    if (result == VK_ERROR_OUT_OF_DATE_KHR)
        return false;
    else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
        throw std::runtime_error("failed to acquire swap chain image!");
    VkSubmitInfo submitInfo = {};
    VkSemaphore waitSemaphores[] = {imageAvailableSemaphore};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    VkSemaphore signalSemaphores[] = {renderFinishedSemaphore};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;
    if (vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS)
        throw std::runtime_error("failed to submit draw command buffer!");
    VkPresentInfoKHR presentInfo = {};
    VkSwapchainKHR swapChains[] = {commandBuffers.graphicsPipeline.renderPass.swapchain.handle};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    result = vkQueuePresentKHR(presentQueue, &presentInfo);
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
        return false;
    else if (result != VK_SUCCESS)
        throw std::runtime_error("failed to present swap chain image!");
    std::cout << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start).count()
              << " microseconds to render" << std::endl;
    glfwWaitEvents();
    vkQueueWaitIdle(presentQueue);
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start).count()
              << " milliseconds frame to frame" << std::endl;
    return true;
}

EditorRenderLoop::EditorRenderLoop(Device& device, const VkSurfaceKHR& surface) : RenderLoop{device, surface}
{
    swapchainRequirements.imageExtent = {80, 45};
    swapchainRequirements.minImageCount = 2;
    swapchainRequirements.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    swapchainRequirements.imageFormat = VK_FORMAT_B8G8R8A8_SRGB;
    swapchainRequirements.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    swapchainRequirements.graphicsQueueFamilyIndices = {0};//TODO honestly anything is an improvement
    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    if (vkCreateSemaphore(device.handle, &semaphoreInfo, nullptr, &imageAvailableSemaphore)
            != VK_SUCCESS
        || vkCreateSemaphore(device.handle, &semaphoreInfo, nullptr, &renderFinishedSemaphore)
            != VK_SUCCESS)
        throw std::runtime_error("failed to create semaphores!");
    vkGetDeviceQueue(device.handle, 0, 0, &graphicsQueue);
    vkGetDeviceQueue(device.handle, 0, 0, &presentQueue);
}

EditorRenderLoop::~EditorRenderLoop()
{
    std::cout<<"RENDERLOOP DESTROYED!"<<std::endl;
    vkDestroySemaphore(swapchainRequirements.device.handle, renderFinishedSemaphore, nullptr);
    vkDestroySemaphore(swapchainRequirements.device.handle, imageAvailableSemaphore, nullptr);
}

std::unique_ptr<RenderLoop> EditorRenderLoop::factory(Device& device, const VkSurfaceKHR& surface)
{
    return std::make_unique<EditorRenderLoop>(device, surface);
}
