#Quick Start Guide

###1 Download Vulkan SDK from https://vulkan.lunarg.com/

###2	Create a work directory
```
$ mkdir vulkan
$ cd vulkan
```
###3	Copy the self-extracting installer to your work directory.
You may need to change the installer file permissions to include execute.

$ cp $HOME/Downloads/vulkansdk-linux-x86_64-\*.run ./
$ chmod ugo+x vulkansdk-linux-x86_64-\*.run

###4	Run the self-extracting installer. The installer won't touch system directories.
Note: You may also be prompted for your sudo password for removal of a previously
installed version of the vulkan-sdk-runtime.

$ ./vulkansdk-linux-x86_64-\*.run

###5 Enter sudo password if prompted

###6	Since SDK installation doesn't copy any libraries to system directory,
some environment variables must be setup. Add the following line to
etc/environment by sudo-ing into your favorite text editor.

VULKAN_SDK="/home/lokko/vulkan/VulkanSDK/{the version number of your Vulkan SDK}/x86_64"

###7 Install some packages
```
$ sudo apt install libxcb1-dev xorg-dev
```
###8	Install GLFW from source instead of installing a package to get the most recent version.
You can find the sources on the official website.
Extract the source code to a convenient directory
and open a terminal in the directory with files like CMakeLists.txt.
Run the following commands to generate a makefile and compile GLFW:
```
$ cmake .
$ make
```
###You may see a warning "Could NOT find Vulkan". Ignore this and continue.
```
$ sudo make install
```
###9  You should now be ready to go.  In case of a problem contact the developer

###PS. This project takes advantage of many c++ 17 features so an up do date compiler is a must.

