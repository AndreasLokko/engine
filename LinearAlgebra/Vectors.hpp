//
// Created by lokko on 23.07.17.
//

#ifndef ENGINE_VECTOR_HPP
#define ENGINE_VECTOR_HPP

#include <array> //typedef uint unsigned int

template<typename T = float>
struct vec2t;

template<typename T = float>
struct vec3t;

template<typename T = float>
struct vec4t;

namespace detail_proxy_vector_class {

template<typename T1, const uint ...swizzles1>
struct swizzleVec {
//region invalid write protection

    template<typename First, typename Second>
    static constexpr bool isUnique(const First& f, const Second& s) {
        return f != s;
    }

    template<typename First, typename Second, typename... Rest>
    static constexpr bool isUnique(const First& f, const Second& s, const Rest&...r) {
        return f != s && isUnique(f, r...);
    }

    template<typename First>
    static constexpr bool areUnique(const First&) {
        return true;
    }

    template<typename First, typename... Rest>
    static constexpr bool areUnique(const First& f, const Rest&...r) {
        return areUnique(r...) && isUnique(f, r...);
    }

    static constexpr bool isWritable() { return areUnique(swizzles1...); }
//endregion invalid write protection

    static constexpr std::array<const uint, sizeof...(swizzles1)> swizzle = {swizzles1...};
    swizzleVec& operator=(const swizzleVec&) = delete;

//region operators

#define CREATE_ASSIGNMENT_OPERATOR(Op)\
                                                                                                                    \
    template<typename T2>                                                                                           \
    static constexpr void unrollAssignmentOp(T1* t1, const T2& t2, uint f){                                         \
        t1[f] Op t2;                                                                                                \
    }                                                                                                               \
                                                                                                                    \
    template<typename T2, typename... Rest>                                                                         \
    static constexpr void unrollAssignmentOp(T1* t1, const T2& t2, uint f, Rest... r){                              \
        t1[f] Op t2;                                                                                                \
        unrollAssignmentOp(t1, t2, r...);                                                                           \
    }                                                                                                               \
                                                                                                                    \
    template<typename T2>                                                                                           \
    static constexpr void unrollAssignmentOp(T1* t1, T2* t2, uint n, uint f){                                       \
        t1[swizzle[n]] Op t2[f];                                                                                    \
    }                                                                                                               \
                                                                                                                    \
    template<typename T2, typename... Rest>                                                                         \
    static constexpr void unrollAssignmentOp(T1* t1 , T2* t2, uint n, uint f, Rest... r){                           \
        t1[swizzle[n]] Op t2[f];                                                                                    \
        unrollAssignmentOp(t1, t2, n+1, r...);                                                                      \
    }                                                                                                               \
                                                                                                                    \
    template<typename T2, const uint... swizzles2>                                                                  \
    typename std::enable_if<isWritable() && (sizeof...(swizzles1) == sizeof...(swizzles2)), swizzleVec>::type&      \
    operator Op (const swizzleVec<T2, swizzles2...>& rhs) {                                                         \
        unrollAssignmentOp((T1*)this, (T2*)(&rhs), 0, swizzles2...);                                                \
        return *this;                                                                                               \
    }                                                                                                               \
                                                                                                                    \
    template<typename T2>                                                                                           \
    typename std::enable_if<isWritable() && std::is_arithmetic<T2>::value, swizzleVec>::type&                       \
    operator Op (const T2& rhs) {                                                                                   \
        unrollAssignmentOp((T1*)this, rhs, swizzles1...);                                                           \
        return *this;                                                                                               \
    }                                                                                                               \

    CREATE_ASSIGNMENT_OPERATOR( = )
#define unrollAssignmentOp unrollAddOp
    CREATE_ASSIGNMENT_OPERATOR( += )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollSubOp
    CREATE_ASSIGNMENT_OPERATOR( -= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollMulOp
    CREATE_ASSIGNMENT_OPERATOR( *= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollDivOp
    CREATE_ASSIGNMENT_OPERATOR( /= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollOrOp
    CREATE_ASSIGNMENT_OPERATOR( |= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollAndOp
    CREATE_ASSIGNMENT_OPERATOR( &= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollXorOp
    CREATE_ASSIGNMENT_OPERATOR( ^= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollRemOp
    CREATE_ASSIGNMENT_OPERATOR( %= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollLshOp
    CREATE_ASSIGNMENT_OPERATOR( <<= )
#undef unrollAssignmentOp
#define unrollAssignmentOp unrollRshOp
    CREATE_ASSIGNMENT_OPERATOR( >>= )
#undef unrollAssignmentOp
#undef CREATE_ASSIGMENT_OPERATOR

typedef typename std::conditional<sizeof...(swizzles1)==2, vec2t<T1>,
                 std::conditional<sizeof...(swizzles1)==3, vec3t<T1>,
                 vec4t<T1>>>::type returnVec;

#define CREATE_ARITHMETIC_OPERATOR(Op)

    template<typename T2, const uint ... swizzles2>
    returnVec operator + (swizzleVec<T2, swizzles2...> rhs) const {
        unrollAddOp((T1*)&rhs, (T2*)this, 0, swizzles2...);
        return rhs;
    }

    template<typename T2>
    friend returnVec operator + (swizzleVec& lhs, const T2& rhs) {
        returnVec v = lhs;
        unrollAddOp((T1*)&v, rhs, swizzles1...);
        return v;
    }

    template<typename T2>
    friend returnVec operator + (const T2& lhs, swizzleVec& rhs) {
        returnVec v = rhs;
        unrollAddOp((T1*)&v, lhs, swizzles1...);
        return v;
    }

#define unrollOp unrollAddOp
        CREATE_ARITHMETIC_OPERATOR(+)
#undef unrollOp
#define unrollOp unrollSubOp
        CREATE_ARITHMETIC_OPERATOR(-)
#undef unrollOp
#define unrollOp unrollMulOp
        CREATE_ARITHMETIC_OPERATOR(*)
#undef unrollOp
#define unrollOp unrollDivOp
        CREATE_ARITHMETIC_OPERATOR(/)
#undef unrollOp
#define unrollOp unrollOrOp
        CREATE_ARITHMETIC_OPERATOR(|)
#undef unrollOp
#define unrollOp unrollAndOp
        CREATE_ARITHMETIC_OPERATOR(&)
#undef unrollOp
#define unrollOp unrollXorOp
        CREATE_ARITHMETIC_OPERATOR(^)
#undef unrollOp
#define unrollOp unrollRemOp
        CREATE_ARITHMETIC_OPERATOR(%)
#undef unrollOp
#define unrollOp unrollLshOp
        CREATE_ARITHMETIC_OPERATOR(<<)
#undef unrollOp
#define unrollOp unrollRshOp
        CREATE_ARITHMETIC_OPERATOR(>>)
#undef unrollOp
#undef CREATE_ARITHMETIC_OPERATOR
//endregion operators
};
}

//region vectors
#define swizzleVec detail_proxy_vector_class::swizzleVec
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

template<typename T>
struct vec2t : public swizzleVec<T,0,1> {
    //static swizzleVec<T,0,1> xy;
    union {
        struct {
            T x;
            T y;
        };
        swizzleVec<T,0,0> xx;
        swizzleVec<T,1,0> yx;
        swizzleVec<T,1,1> yy;
        swizzleVec<T,0,0,0> xxx;
        swizzleVec<T,0,0,1> xxy;
        swizzleVec<T,0,1,0> xyx;
        swizzleVec<T,0,1,1> xyy;
        swizzleVec<T,1,0,0> yxx;
        swizzleVec<T,1,0,1> yxy;
        swizzleVec<T,1,1,0> yyx;
        swizzleVec<T,1,1,1> yyy;
        swizzleVec<T,0,0,0,0> xxxx;
        swizzleVec<T,0,0,0,1> xxxy;
        swizzleVec<T,0,0,1,0> xxyx;
        swizzleVec<T,0,0,1,1> xxyy;
        swizzleVec<T,0,1,0,0> xyxx;
        swizzleVec<T,0,1,0,1> xyxy;
        swizzleVec<T,0,1,1,0> xyyx;
        swizzleVec<T,0,1,1,1> xyyy;
        swizzleVec<T,1,0,0,0> yxxx;
        swizzleVec<T,1,0,0,1> yxxy;
        swizzleVec<T,1,0,1,0> yxyx;
        swizzleVec<T,1,0,1,1> yxyy;
        swizzleVec<T,1,1,0,0> yyxx;
        swizzleVec<T,1,1,0,1> yyxy;
        swizzleVec<T,1,1,1,0> yyyx;
        swizzleVec<T,1,1,1,1> yyyy;
    };

    vec2t& operator = (vec2t v){x = v.x; y = v.y; return *this;}

    template<uint a, uint b>
    vec2t(const swizzleVec<T,a,b>& v){x = ((T*)&v)[a]; y = ((T*)&v)[b];}

    template<typename T1, typename T2>
    vec2t<T>(T1 X, T2 Y): x(X), y(Y) {}

    template<typename T1>
    explicit vec2t(const vec2t<T1>& xy): x(xy.x), y(xy.y) {}

    vec2t(){};
};

template<typename T>
struct vec3t : public swizzleVec<T,0,1,2> {
    //static swizzleVec<T,0,1,2> xyz;
    union {
        struct {
            T x;
            T y;
            T z;
        };
        swizzleVec<T,0,0> xx;
        swizzleVec<T,0,1> xy;
        swizzleVec<T,0,2> xz;
        swizzleVec<T,1,0> yx;
        swizzleVec<T,1,1> yy;
        swizzleVec<T,1,2> yz;
        swizzleVec<T,2,0> zx;
        swizzleVec<T,2,1> zy;
        swizzleVec<T,2,2> zz;
        swizzleVec<T,0,0,0> xxx;
        swizzleVec<T,0,0,1> xxy;
        swizzleVec<T,0,0,2> xxz;
        swizzleVec<T,0,1,0> xyx;
        swizzleVec<T,0,1,1> xyy;
        swizzleVec<T,0,2,0> xzx;
        swizzleVec<T,0,2,1> xzy;
        swizzleVec<T,0,2,2> xzz;
        swizzleVec<T,1,0,0> yxx;
        swizzleVec<T,1,0,1> yxy;
        swizzleVec<T,1,0,2> yxz;
        swizzleVec<T,1,1,0> yyx;
        swizzleVec<T,1,1,1> yyy;
        swizzleVec<T,1,1,2> yyz;
        swizzleVec<T,1,2,0> yzx;
        swizzleVec<T,1,2,1> yzy;
        swizzleVec<T,1,2,2> yzz;
        swizzleVec<T,2,0,0> zxx;
        swizzleVec<T,2,0,1> zxy;
        swizzleVec<T,2,0,2> zxz;
        swizzleVec<T,2,1,0> zyx;
        swizzleVec<T,2,1,1> zyy;
        swizzleVec<T,2,1,2> zyz;
        swizzleVec<T,2,2,0> zzx;
        swizzleVec<T,2,2,1> zzy;
        swizzleVec<T,2,2,2> zzz;
        swizzleVec<T,0,0,0,0> xxxx;
        swizzleVec<T,0,0,0,1> xxxy;
        swizzleVec<T,0,0,0,2> xxxz;
        swizzleVec<T,0,0,1,0> xxyx;
        swizzleVec<T,0,0,1,1> xxyy;
        swizzleVec<T,0,0,1,2> xxyz;
        swizzleVec<T,0,0,2,0> xxzx;
        swizzleVec<T,0,0,2,1> xxzy;
        swizzleVec<T,0,0,2,2> xxzz;
        swizzleVec<T,0,1,0,0> xyxx;
        swizzleVec<T,0,1,0,1> xyxy;
        swizzleVec<T,0,1,0,2> xyxz;
        swizzleVec<T,0,1,1,0> xyyx;
        swizzleVec<T,0,1,1,1> xyyy;
        swizzleVec<T,0,1,1,2> xyyz;
        swizzleVec<T,0,1,2,0> xyzx;
        swizzleVec<T,0,1,2,1> xyzy;
        swizzleVec<T,0,1,2,2> xyzz;
        swizzleVec<T,0,2,0,0> xzxx;
        swizzleVec<T,0,2,0,1> xzxy;
        swizzleVec<T,0,2,0,2> xzxz;
        swizzleVec<T,0,2,1,0> xzyx;
        swizzleVec<T,0,2,1,1> xzyy;
        swizzleVec<T,0,2,1,2> xzyz;
        swizzleVec<T,0,2,2,0> xzzx;
        swizzleVec<T,0,2,2,1> xzzy;
        swizzleVec<T,0,2,2,2> xzzz;
        swizzleVec<T,1,0,0,0> yxxx;
        swizzleVec<T,1,0,0,1> yxxy;
        swizzleVec<T,1,0,0,2> yxxz;
        swizzleVec<T,1,0,1,0> yxyx;
        swizzleVec<T,1,0,1,1> yxyy;
        swizzleVec<T,1,0,1,2> yxyz;
        swizzleVec<T,1,0,2,0> yxzx;
        swizzleVec<T,1,0,2,1> yxzy;
        swizzleVec<T,1,0,2,2> yxzz;
        swizzleVec<T,1,1,0,0> yyxx;
        swizzleVec<T,1,1,0,1> yyxy;
        swizzleVec<T,1,1,0,2> yyxz;
        swizzleVec<T,1,1,1,0> yyyx;
        swizzleVec<T,1,1,1,1> yyyy;
        swizzleVec<T,1,1,1,2> yyyz;
        swizzleVec<T,1,1,2,0> yyzx;
        swizzleVec<T,1,1,2,1> yyzy;
        swizzleVec<T,1,1,2,2> yyzz;
        swizzleVec<T,1,2,0,0> yzxx;
        swizzleVec<T,1,2,0,1> yzxy;
        swizzleVec<T,1,2,0,2> yzxz;
        swizzleVec<T,1,2,1,0> yzyx;
        swizzleVec<T,1,2,1,1> yzyy;
        swizzleVec<T,1,2,1,2> yzyz;
        swizzleVec<T,1,2,2,0> yzzx;
        swizzleVec<T,1,2,2,1> yzzy;
        swizzleVec<T,1,2,2,2> yzzz;
        swizzleVec<T,2,0,0,0> zxxx;
        swizzleVec<T,2,0,0,1> zxxy;
        swizzleVec<T,2,0,0,2> zxxz;
        swizzleVec<T,2,0,1,0> zxyx;
        swizzleVec<T,2,0,1,1> zxyy;
        swizzleVec<T,2,0,1,2> zxyz;
        swizzleVec<T,2,0,2,0> zxzx;
        swizzleVec<T,2,0,2,1> zxzy;
        swizzleVec<T,2,0,2,2> zxzz;
        swizzleVec<T,2,1,0,0> zyxx;
        swizzleVec<T,2,1,0,1> zyxy;
        swizzleVec<T,2,1,0,2> zyxz;
        swizzleVec<T,2,1,1,0> zyyx;
        swizzleVec<T,2,1,1,1> zyyy;
        swizzleVec<T,2,1,1,2> zyyz;
        swizzleVec<T,2,1,2,0> zyzx;
        swizzleVec<T,2,1,2,1> zyzy;
        swizzleVec<T,2,1,2,2> zyzz;
        swizzleVec<T,2,2,0,0> zzxx;
        swizzleVec<T,2,2,0,1> zzxy;
        swizzleVec<T,2,2,0,2> zzxz;
        swizzleVec<T,2,2,1,0> zzyx;
        swizzleVec<T,2,2,1,1> zzyy;
        swizzleVec<T,2,2,1,2> zzyz;
        swizzleVec<T,2,2,2,0> zzzx;
        swizzleVec<T,2,2,2,1> zzzy;
        swizzleVec<T,1,2,2,2> zzzz;
    };

    vec3t& operator = (vec3t v){x = v.x; y = v.y; z = v.z; return *this;}

    template<uint a, uint b, uint c>
    vec3t(const swizzleVec<T,a,b,c>& v){x = ((T*)&v)[a]; y = ((T*)&v)[b]; z = ((T*)&v)[c];}

    template<typename T1, typename T2, typename T3>
    vec3t(T1 X, T2 Y, T3 Z): x(X), y(Y), z(Z) {}

    template<typename T1, typename T2>
    vec3t(T1 X, vec2t<T2> YZ): x(X), y(YZ.x), z(YZ.y) {}

    template<typename T1, typename T2>
    vec3t(vec2t<T1> XY, T2 Z): x(XY.x), y(XY.y), z(Z) {}

    template<typename T1>
    explicit vec3t(const vec3t<T1>& XYZ): x(XYZ.z), y(XYZ.y), z(XYZ.z) {}

    vec3t(){}
};

template<typename T>
struct vec4t : public swizzleVec<T,0,1,2,3> {
    //static swizzleVec<T,0,1,2,3> xyzw;
    union {
        struct {
            T x;
            T y;
            T z;
            T w;
        };
        swizzleVec<T,0,0> xx;
        swizzleVec<T,0,1> xy;
        swizzleVec<T,0,2> xz;
        swizzleVec<T,0,3> xw;
        swizzleVec<T,1,0> yx;
        swizzleVec<T,1,1> yy;
        swizzleVec<T,1,2> yz;
        swizzleVec<T,1,3> yw;
        swizzleVec<T,2,0> zx;
        swizzleVec<T,2,1> zy;
        swizzleVec<T,2,2> zz;
        swizzleVec<T,2,3> zw;
        swizzleVec<T,3,0> wx;
        swizzleVec<T,3,1> wy;
        swizzleVec<T,3,2> wz;
        swizzleVec<T,3,3> ww;
        swizzleVec<T,0,0,0> xxx;
        swizzleVec<T,0,0,1> xxy;
        swizzleVec<T,0,0,2> xxz;
        swizzleVec<T,0,0,3> xxw;
        swizzleVec<T,0,1,0> xyx;
        swizzleVec<T,0,1,1> xyy;
        swizzleVec<T,0,1,2> xyz;
        swizzleVec<T,0,1,3> xyw;
        swizzleVec<T,0,2,0> xzx;
        swizzleVec<T,0,2,1> xzy;
        swizzleVec<T,0,2,2> xzz;
        swizzleVec<T,0,2,3> xzw;
        swizzleVec<T,0,3,0> xwx;
        swizzleVec<T,0,3,1> xwy;
        swizzleVec<T,0,3,2> xwz;
        swizzleVec<T,0,3,3> xww;
        swizzleVec<T,1,0,0> yxx;
        swizzleVec<T,1,0,1> yxy;
        swizzleVec<T,1,0,2> yxz;
        swizzleVec<T,1,0,3> yxw;
        swizzleVec<T,1,1,0> yyx;
        swizzleVec<T,1,1,1> yyy;
        swizzleVec<T,1,1,2> yyz;
        swizzleVec<T,1,1,3> yyw;
        swizzleVec<T,1,2,0> yzx;
        swizzleVec<T,1,2,1> yzy;
        swizzleVec<T,1,2,2> yzz;
        swizzleVec<T,1,2,3> yzw;
        swizzleVec<T,1,3,0> ywx;
        swizzleVec<T,1,3,1> ywy;
        swizzleVec<T,1,3,2> ywz;
        swizzleVec<T,1,3,3> yww;
        swizzleVec<T,2,0,0> zxx;
        swizzleVec<T,2,0,1> zxy;
        swizzleVec<T,2,0,2> zxz;
        swizzleVec<T,2,0,3> zxw;
        swizzleVec<T,2,1,0> zyx;
        swizzleVec<T,2,1,1> zyy;
        swizzleVec<T,2,1,2> zyz;
        swizzleVec<T,2,1,3> zyw;
        swizzleVec<T,2,2,0> zzx;
        swizzleVec<T,2,2,1> zzy;
        swizzleVec<T,2,2,2> zzz;
        swizzleVec<T,2,2,3> zzw;
        swizzleVec<T,2,3,0> zwx;
        swizzleVec<T,2,3,1> zwy;
        swizzleVec<T,2,3,2> zwz;
        swizzleVec<T,2,3,3> zww;
        swizzleVec<T,3,0,0> wxx;
        swizzleVec<T,3,0,1> wxy;
        swizzleVec<T,3,0,2> wxz;
        swizzleVec<T,3,0,3> wxw;
        swizzleVec<T,3,1,0> wyx;
        swizzleVec<T,3,1,1> wyy;
        swizzleVec<T,3,1,2> wyz;
        swizzleVec<T,3,1,3> wyw;
        swizzleVec<T,3,2,0> wzx;
        swizzleVec<T,3,2,1> wzy;
        swizzleVec<T,3,2,2> wzz;
        swizzleVec<T,3,2,3> wzw;
        swizzleVec<T,3,3,0> wwx;
        swizzleVec<T,3,3,1> wwy;
        swizzleVec<T,3,3,2> wwz;
        swizzleVec<T,3,3,3> www;
        swizzleVec<T,0,0,0,0> xxxx;
        swizzleVec<T,0,0,0,1> xxxy;
        swizzleVec<T,0,0,0,2> xxxz;
        swizzleVec<T,0,0,0,3> xxxw;
        swizzleVec<T,0,0,1,0> xxyx;
        swizzleVec<T,0,0,1,1> xxyy;
        swizzleVec<T,0,0,1,2> xxyz;
        swizzleVec<T,0,0,1,3> xxyw;
        swizzleVec<T,0,0,2,0> xxzx;
        swizzleVec<T,0,0,2,1> xxzy;
        swizzleVec<T,0,0,2,2> xxzz;
        swizzleVec<T,0,0,2,3> xxzw;
        swizzleVec<T,0,0,3,0> xxwx;
        swizzleVec<T,0,0,3,1> xxwy;
        swizzleVec<T,0,0,3,2> xxwz;
        swizzleVec<T,0,0,3,3> xxww;
        swizzleVec<T,0,1,0,0> xyxx;
        swizzleVec<T,0,1,0,1> xyxy;
        swizzleVec<T,0,1,0,2> xyxz;
        swizzleVec<T,0,1,0,3> xyxw;
        swizzleVec<T,0,1,1,0> xyyx;
        swizzleVec<T,0,1,1,1> xyyy;
        swizzleVec<T,0,1,1,2> xyyz;
        swizzleVec<T,0,1,1,3> xyyw;
        swizzleVec<T,0,1,2,0> xyzx;
        swizzleVec<T,0,1,2,1> xyzy;
        swizzleVec<T,0,1,2,2> xyzz;
        swizzleVec<T,0,1,3,0> xywx;
        swizzleVec<T,0,1,3,1> xywy;
        swizzleVec<T,0,1,3,2> xywz;
        swizzleVec<T,0,1,3,3> xyww;
        swizzleVec<T,0,2,0,0> xzxx;
        swizzleVec<T,0,2,0,1> xzxy;
        swizzleVec<T,0,2,0,2> xzxz;
        swizzleVec<T,0,2,0,3> xzxw;
        swizzleVec<T,0,2,1,0> xzyx;
        swizzleVec<T,0,2,1,1> xzyy;
        swizzleVec<T,0,2,1,2> xzyz;
        swizzleVec<T,0,2,1,3> xzyw;
        swizzleVec<T,0,2,2,0> xzzx;
        swizzleVec<T,0,2,2,1> xzzy;
        swizzleVec<T,0,2,2,2> xzzz;
        swizzleVec<T,0,2,2,3> xzzw;
        swizzleVec<T,0,2,3,0> xzwx;
        swizzleVec<T,0,2,3,1> xzwy;
        swizzleVec<T,0,2,3,2> xzwz;
        swizzleVec<T,0,2,3,3> xzww;
        swizzleVec<T,0,3,0,0> xwxx;
        swizzleVec<T,0,3,0,1> xwxy;
        swizzleVec<T,0,3,0,2> xwxz;
        swizzleVec<T,0,3,0,3> xwxw;
        swizzleVec<T,0,3,1,0> xwyx;
        swizzleVec<T,0,3,1,1> xwyy;
        swizzleVec<T,0,3,1,2> xwyz;
        swizzleVec<T,0,3,1,3> xwyw;
        swizzleVec<T,0,3,2,0> xwzx;
        swizzleVec<T,0,3,2,1> xwzy;
        swizzleVec<T,0,3,2,2> xwzz;
        swizzleVec<T,0,3,2,3> xwzw;
        swizzleVec<T,0,3,3,0> xwwx;
        swizzleVec<T,0,3,3,1> xwwy;
        swizzleVec<T,0,3,3,2> xwwz;
        swizzleVec<T,0,3,3,3> xwww;
        swizzleVec<T,1,0,0,0> yxxx;
        swizzleVec<T,1,0,0,1> yxxy;
        swizzleVec<T,1,0,0,2> yxxz;
        swizzleVec<T,1,0,0,3> yxxw;
        swizzleVec<T,1,0,1,0> yxyx;
        swizzleVec<T,1,0,1,1> yxyy;
        swizzleVec<T,1,0,1,2> yxyz;
        swizzleVec<T,1,0,1,3> yxyw;
        swizzleVec<T,1,0,2,0> yxzx;
        swizzleVec<T,1,0,2,1> yxzy;
        swizzleVec<T,1,0,2,2> yxzz;
        swizzleVec<T,1,0,2,3> yxzw;
        swizzleVec<T,1,0,3,0> yxwx;
        swizzleVec<T,1,0,3,1> yxwy;
        swizzleVec<T,1,0,3,2> yxwz;
        swizzleVec<T,1,0,3,3> yxww;
        swizzleVec<T,1,1,0,0> yyxx;
        swizzleVec<T,1,1,0,1> yyxy;
        swizzleVec<T,1,1,0,2> yyxz;
        swizzleVec<T,1,1,0,3> yyxw;
        swizzleVec<T,1,1,1,0> yyyx;
        swizzleVec<T,1,1,1,1> yyyy;
        swizzleVec<T,1,1,1,2> yyyz;
        swizzleVec<T,1,1,1,3> yyyw;
        swizzleVec<T,1,1,2,0> yyzx;
        swizzleVec<T,1,1,2,1> yyzy;
        swizzleVec<T,1,1,2,2> yyzz;
        swizzleVec<T,1,1,2,3> yyzw;
        swizzleVec<T,1,1,3,0> yywx;
        swizzleVec<T,1,1,3,1> yywy;
        swizzleVec<T,1,1,3,2> yywz;
        swizzleVec<T,1,1,3,3> yyww;
        swizzleVec<T,1,2,0,0> yzxx;
        swizzleVec<T,1,2,0,1> yzxy;
        swizzleVec<T,1,2,0,2> yzxz;
        swizzleVec<T,1,2,0,3> yzxw;
        swizzleVec<T,1,2,1,0> yzyx;
        swizzleVec<T,1,2,1,1> yzyy;
        swizzleVec<T,1,2,1,2> yzyz;
        swizzleVec<T,1,2,1,3> yzyw;
        swizzleVec<T,1,2,2,0> yzzx;
        swizzleVec<T,1,2,2,1> yzzy;
        swizzleVec<T,1,2,2,2> yzzz;
        swizzleVec<T,1,2,2,3> yzzw;
        swizzleVec<T,1,2,3,0> yzwx;
        swizzleVec<T,1,2,3,1> yzwy;
        swizzleVec<T,1,2,3,2> yzwz;
        swizzleVec<T,1,2,3,3> yzww;
        swizzleVec<T,1,3,0,0> ywxx;
        swizzleVec<T,1,3,0,1> ywxy;
        swizzleVec<T,1,3,0,2> ywxz;
        swizzleVec<T,1,3,0,3> ywxw;
        swizzleVec<T,1,3,1,0> ywyx;
        swizzleVec<T,1,3,1,1> ywyy;
        swizzleVec<T,1,3,1,2> ywyz;
        swizzleVec<T,1,3,1,3> ywyw;
        swizzleVec<T,1,3,2,0> ywzx;
        swizzleVec<T,1,3,2,1> ywzy;
        swizzleVec<T,1,3,2,2> ywzz;
        swizzleVec<T,1,3,2,3> ywzw;
        swizzleVec<T,1,3,3,0> ywwx;
        swizzleVec<T,1,3,3,1> ywwy;
        swizzleVec<T,1,3,3,2> ywwz;
        swizzleVec<T,1,3,3,3> ywww;
        swizzleVec<T,2,0,0,0> zxxx;
        swizzleVec<T,2,0,0,1> zxxy;
        swizzleVec<T,2,0,0,2> zxxz;
        swizzleVec<T,2,0,0,3> zxxw;
        swizzleVec<T,2,0,1,0> zxyx;
        swizzleVec<T,2,0,1,1> zxyy;
        swizzleVec<T,2,0,1,2> zxyz;
        swizzleVec<T,2,0,1,3> zxyw;
        swizzleVec<T,2,0,2,0> zxzx;
        swizzleVec<T,2,0,2,1> zxzy;
        swizzleVec<T,2,0,2,2> zxzz;
        swizzleVec<T,2,0,2,3> zxzw;
        swizzleVec<T,2,0,3,0> zxwx;
        swizzleVec<T,2,0,3,1> zxwy;
        swizzleVec<T,2,0,3,2> zxwz;
        swizzleVec<T,2,0,3,3> zxww;
        swizzleVec<T,2,1,0,0> zyxx;
        swizzleVec<T,2,1,0,1> zyxy;
        swizzleVec<T,2,1,0,2> zyxz;
        swizzleVec<T,2,1,0,3> zyxw;
        swizzleVec<T,2,1,1,0> zyyx;
        swizzleVec<T,2,1,1,1> zyyy;
        swizzleVec<T,2,1,1,2> zyyz;
        swizzleVec<T,2,1,1,3> zyyw;
        swizzleVec<T,2,1,2,0> zyzx;
        swizzleVec<T,2,1,2,1> zyzy;
        swizzleVec<T,2,1,2,2> zyzz;
        swizzleVec<T,2,1,2,3> zyzw;
        swizzleVec<T,2,1,3,0> zywx;
        swizzleVec<T,2,1,3,1> zywy;
        swizzleVec<T,2,1,3,2> zywz;
        swizzleVec<T,2,1,3,3> zyww;
        swizzleVec<T,2,2,0,0> zzxx;
        swizzleVec<T,2,2,0,1> zzxy;
        swizzleVec<T,2,2,0,2> zzxz;
        swizzleVec<T,2,2,0,3> zzxw;
        swizzleVec<T,2,2,1,0> zzyx;
        swizzleVec<T,2,2,1,1> zzyy;
        swizzleVec<T,2,2,1,2> zzyz;
        swizzleVec<T,2,2,1,3> zzyw;
        swizzleVec<T,2,2,2,0> zzzx;
        swizzleVec<T,2,2,2,1> zzzy;
        swizzleVec<T,2,2,2,2> zzzz;
        swizzleVec<T,2,2,2,3> zzzw;
        swizzleVec<T,2,2,3,0> zzwx;
        swizzleVec<T,2,2,3,1> zzwy;
        swizzleVec<T,2,2,3,2> zzwz;
        swizzleVec<T,2,2,3,3> zzww;
        swizzleVec<T,2,3,0,0> zwxx;
        swizzleVec<T,2,3,0,1> zwxy;
        swizzleVec<T,2,3,0,2> zwxz;
        swizzleVec<T,2,3,0,3> zwxw;
        swizzleVec<T,2,3,1,0> zwyx;
        swizzleVec<T,2,3,1,1> zwyy;
        swizzleVec<T,2,3,1,2> zwyz;
        swizzleVec<T,2,3,1,3> zwyw;
        swizzleVec<T,2,3,2,0> zwzx;
        swizzleVec<T,2,3,2,1> zwzy;
        swizzleVec<T,2,3,2,2> zwzz;
        swizzleVec<T,2,3,2,3> zwzw;
        swizzleVec<T,2,3,3,0> zwwx;
        swizzleVec<T,2,3,3,1> zwwy;
        swizzleVec<T,2,3,3,2> zwwz;
        swizzleVec<T,2,3,3,3> zwww;
        swizzleVec<T,3,0,0,0> wxxx;
        swizzleVec<T,3,0,0,1> wxxy;
        swizzleVec<T,3,0,0,2> wxxz;
        swizzleVec<T,3,0,0,3> wxxw;
        swizzleVec<T,3,0,1,0> wxyx;
        swizzleVec<T,3,0,1,1> wxyy;
        swizzleVec<T,3,0,1,2> wxyz;
        swizzleVec<T,3,0,1,3> wxyw;
        swizzleVec<T,3,0,2,0> wxzx;
        swizzleVec<T,3,0,2,1> wxzy;
        swizzleVec<T,3,0,2,2> wxzz;
        swizzleVec<T,3,0,2,3> wxzw;
        swizzleVec<T,3,0,3,0> wxwx;
        swizzleVec<T,3,0,3,1> wxwy;
        swizzleVec<T,3,0,3,2> wxwz;
        swizzleVec<T,3,0,3,3> wxww;
        swizzleVec<T,3,1,0,0> wyxx;
        swizzleVec<T,3,1,0,1> wyxy;
        swizzleVec<T,3,1,0,2> wyxz;
        swizzleVec<T,3,1,0,3> wyxw;
        swizzleVec<T,3,1,1,0> wyyx;
        swizzleVec<T,3,1,1,1> wyyy;
        swizzleVec<T,3,1,1,2> wyyz;
        swizzleVec<T,3,1,1,3> wyyw;
        swizzleVec<T,3,1,2,0> wyzx;
        swizzleVec<T,3,1,2,1> wyzy;
        swizzleVec<T,3,1,2,2> wyzz;
        swizzleVec<T,3,1,2,3> wyzw;
        swizzleVec<T,3,1,3,0> wywx;
        swizzleVec<T,3,1,3,1> wywy;
        swizzleVec<T,3,1,3,2> wywz;
        swizzleVec<T,3,1,3,3> wyww;
        swizzleVec<T,3,2,0,0> wzxx;
        swizzleVec<T,3,2,0,1> wzxy;
        swizzleVec<T,3,2,0,2> wzxz;
        swizzleVec<T,3,2,0,3> wzxw;
        swizzleVec<T,3,2,1,0> wzyx;
        swizzleVec<T,3,2,1,1> wzyy;
        swizzleVec<T,3,2,1,2> wzyz;
        swizzleVec<T,3,2,1,3> wzyw;
        swizzleVec<T,3,2,2,0> wzzx;
        swizzleVec<T,3,2,2,1> wzzy;
        swizzleVec<T,3,2,2,2> wzzz;
        swizzleVec<T,3,2,2,3> wzzw;
        swizzleVec<T,3,2,3,0> wzwx;
        swizzleVec<T,3,2,3,1> wzwy;
        swizzleVec<T,3,2,3,2> wzwz;
        swizzleVec<T,3,2,3,3> wzww;
        swizzleVec<T,3,3,0,0> wwxx;
        swizzleVec<T,3,3,0,1> wwxy;
        swizzleVec<T,3,3,0,2> wwxz;
        swizzleVec<T,3,3,0,3> wwxw;
        swizzleVec<T,3,3,1,0> wwyx;
        swizzleVec<T,3,3,1,1> wwyy;
        swizzleVec<T,3,3,1,2> wwyz;
        swizzleVec<T,3,3,1,3> wwyw;
        swizzleVec<T,3,3,2,0> wwzx;
        swizzleVec<T,3,3,2,1> wwzy;
        swizzleVec<T,3,3,2,2> wwzz;
        swizzleVec<T,3,3,2,3> wwzw;
        swizzleVec<T,3,3,3,0> wwwx;
        swizzleVec<T,3,3,3,1> wwwy;
        swizzleVec<T,3,3,3,2> wwwz;
        swizzleVec<T,3,3,3,3> wwww;
    };

    vec4t& operator = (vec4t v){x = v.x; y = v.y; z = v.z; w = v.w; return *this;}

    template<uint a, uint b, uint c, uint d>
    vec4t(const swizzleVec<T,a,b,c,d>& v) {x = ((T*)&v)[a]; y = ((T*)&v)[b]; z = ((T*)&v)[c]; w = ((T*)&v)[d];}

    template<typename T1, typename T2, typename T3, typename  T4>
    vec4t(T1 X, T2 Y, T3 Z, T4 W): x(X), y(Y), z(Z), w(W) {}

    template<typename T1, typename T2, typename T3>
    vec4t(vec2t<T1> XY, T2 Z, T3 W): x(XY.x), y(XY.y), z(Z), w(W) {}

    template<typename T1, typename T2, typename T3>
    vec4t(T1 X, vec2t<T2> YZ, T3 W): x(X), y(YZ.x), z(YZ.y), w(W)  {}

    template<typename T1, typename T2, typename T3>
    vec4t(T1 X, T2 Y, vec2t<T3> ZW): x(X), y(Y), z(ZW.x), w(ZW.y) {}

    template<typename T1, typename T2>
    vec4t(vec3t<T1>& XYZ, T2 W): x(XYZ.x), y(XYZ.y), z(XYZ.z), w(W)  {}

    template<typename T1, typename T2>
    vec4t(T1 X, vec3t<T2> YZW): x(X), y(YZW.x), z(YZW.y), w(YZW.z)  {}

    template<typename T1>
    vec4t(vec4t<T1>& XYZW): x(XYZW.x), y(XYZW.y), z(XYZW.z), w(XYZW.w)  {}

    vec4t() {}
};

typedef vec2t<> vec2;
typedef vec3t<> vec3;
typedef vec4t<> vec4;

#pragma GCC diagnostic pop
#undef swizzleVec
//endregion vectors
#endif //ENGINE_VECTOR_HPP