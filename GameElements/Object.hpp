//
// Created by lokko on 14.08.17.
//

#ifndef ENGINE_OBJECT_HPP
#define ENGINE_OBJECT_HPP

#include "../LinearAlgebra/Vectors.hpp"

class Object {
    vec3t<__int128_t> position;
    vec3 velocity;
    vec3 rotation;
    vec3 angularVelocity;
    vec3 PMoI; //principal moments of inertia
    float mass;
    uint32_t padding;
    //104 bytes using 8 byte alignment
};

#endif //ENGINE_OBJECT_HPP
