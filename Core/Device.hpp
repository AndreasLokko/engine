//
// Created by lokko on 7.07.17.
//

#ifndef ENGINE_DEVICEMANAGER_HPP
#define ENGINE_DEVICEMANAGER_HPP

#include "glfw3.hpp"
#include "PhysicalDevice.hpp"
#include <map>
#include <vector>

struct DeviceRequirements
{
    struct QueueRequirement
    {
        VkQueueFlags flags;
        bool present;
    };
    DeviceRequirements(const VkInstance& _instance);
    void addQueueRequirement(const QueueRequirement& qRequirement);
    VkInstance instance;
private:
    std::vector<QueueRequirement> queueRequirements;
};

class Device {
    std::vector<std::vector<bool>> queueAllocated;
public:
    Device(const PhysicalDevice& physicalDevice);
    VkDevice handle;
    std::vector<bool> present;
    VkCommandPool commandPool;
    const VkPhysicalDevice& physicalDevice;
    std::vector<VkQueueFamilyProperties> queueFamilies;//TODO const
    ~Device();
};

#endif //ENGINE_DEVICEMANAGER_HPP