//
// Created by lokko on 2.07.17.
//

#ifndef ENGINE_APPLICATIONMANAGER_HPP
#define ENGINE_APPLICATIONMANAGER_HPP

#include "glfw3.hpp"

void ApplicationManager(VkInstance&&);

#endif //ENGINE_APPLICATIONMANAGER_HPP
