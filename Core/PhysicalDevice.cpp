//
// Created by lokko on 8.07.17.
//

#include <stdexcept>
#include <iostream>
#include <mutex>
#include "PhysicalDevice.hpp"

struct PhysicalDevices : std::vector<PhysicalDevice>
{
    PhysicalDevices(){};
    void populate(VkInstance instance)
    {
        uint32_t deviceCount;
        if(vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr) != VK_SUCCESS)
            throw std::runtime_error("failed to find devices with Vulkan support!");
        std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
        vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices.data());
        reserve(deviceCount);
        for (const VkPhysicalDevice& physicalDevice : physicalDevices)
            push_back({physicalDevice, instance});
    }
};

static PhysicalDevices physicalDevices;

static std::once_flag flag;

void _collectPhysicalDevices(const VkInstance &instance, int line, const char* file)
{
    bool run = true;
    std::call_once(flag,
    [&]{
        physicalDevices.populate(instance);
        run = false;
    });
    if(run)
        throw std::runtime_error(
                "In file " + (std::string)file + ": line " + std::to_string(line) + "\n PhysicalDevices have already been collected!");
}

PhysicalDevice reserveDevice()
{
    for (auto& physicalDevice : physicalDevices) {
        if (physicalDevice.deviceReserved)
            continue;
        physicalDevice.deviceInUse = true;
        physicalDevice.deviceReserved = true;
        return physicalDevice;
    }
    return PhysicalDevice();
}


PhysicalDevice useDevice()
{
    for (auto& physicalDevice : physicalDevices) {
        if (physicalDevice.deviceReserved)
            continue;
        physicalDevice.deviceInUse = true;
        return physicalDevice;
    }
    return PhysicalDevice();
}

PhysicalDevice::QueueFamily::QueueFamily(const VkQueueFamilyProperties &properties, int presentable)
        : properties(properties),
          presentable(presentable)
{}

PhysicalDevice::PhysicalDevice(VkPhysicalDevice handle, VkInstance instance)
        : handle(handle),
          deviceReserved(),
          deviceInUse(),
          queueFamilies(handle, instance)
{}

PhysicalDevice::PhysicalDevice()
        : handle(VK_NULL_HANDLE)
{}

PhysicalDevice::QueueFamilies::QueueFamilies(VkPhysicalDevice handle, VkInstance instance)
{
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(handle, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(handle, &queueFamilyCount, queueFamilyProperties.data());
    reserve(queueFamilyCount);
    uint32_t i = 0;
    for (const auto& queueFamilyProperty : queueFamilyProperties)
        if (queueFamilyProperty.queueCount > 0)
            push_back(
                    {
                        queueFamilyProperty,
                        glfwGetPhysicalDevicePresentationSupport(instance, handle, i++)
                    });
}

PhysicalDevice::QueueFamilies::QueueFamilies() {}

//TODO check if device is suitable

//bool isDeviceSuitable(VkPhysicalDevice device) {
//    QueueFamilyIndices indices = findQueueFamilies(device);
//
//    bool extensionsSupported = checkDeviceExtensionSupport(device);
//
//    bool swapChainAdequate = false;
//    if (extensionsSupported) {
//        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
//        swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
//    }
//
//    return indices.isComplete() && extensionsSupported && swapChainAdequate;
//}
//
//bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
//    uint32_t extensionCount;
//    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
//
//    std::vector<VkExtensionProperties> availableExtensions(extensionCount);
//    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());
//
//    std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());
//
//    for (const auto& extension : availableExtensions) {
//        requiredExtensions.erase(extension.extensionName);
//    }
//
//    return requiredExtensions.empty();
//}