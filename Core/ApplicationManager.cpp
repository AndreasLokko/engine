//
// Created by lokko on 2.07.17.
//

#include "ApplicationManager.hpp"
#include "ValidationLayers.hpp"
#include "PhysicalDevice.hpp"
#include "Application.hpp"
#include <iostream>

struct Extensions : std::vector<const char*>
{
    Extensions()
    {
        unsigned int glfwExtensionCount;
        const char** glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
        reserve(glfwExtensionCount + ValidationLayers::enableValidationLayers);
        assign(glfwExtensions, glfwExtensions + glfwExtensionCount);
        if (ValidationLayers::enableValidationLayers)
            push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }
};

void printInitializedGLFW()
{
    std::cout << "GLFW initialised to version ";
    int major, minor, rev;
    glfwGetVersion(&major,&minor,&rev);
    std::cout << major << "." << minor << "." << rev << std::endl;
    if (glfwVulkanSupported() == GLFW_TRUE)
        std::cout << "GLFW has Vulkan support" << std::endl;
}

void initGLFW()
{
    if (glfwInit() != GLFW_TRUE)
        std::cout<< "GLFW initialisation failure" << std::endl;
    else
        printInitializedGLFW();
}

void createInstance(VkInstance& instance)
{
    VkApplicationInfo appInfo = {};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pApplicationName = "GameEngine";
        appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.pEngineName = "Engine";
        appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.apiVersion = VK_API_VERSION_1_0;
    VkInstanceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &appInfo;
        createInfo.enabledLayerCount = 0;
    if (ValidationLayers::enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(ValidationLayers::validationLayers.size());
        createInfo.ppEnabledLayerNames = ValidationLayers::validationLayers.data();
    }
    Extensions extensions;
        createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        createInfo.ppEnabledExtensionNames = extensions.data();
    if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS)
        throw std::runtime_error("failed to create instance!");
}

void ApplicationManager(VkInstance&& instance)
{
    initGLFW();
    createInstance(instance);
    ValidationLayers validationLayers(instance);
    validationLayers.checkValidationLayerSupport();
    validationLayers.setupDebugCallback();
    collectPhysicalDevices(instance);
    Application(instance);
    glfwTerminate();
    vkDestroyInstance(instance, nullptr);
}