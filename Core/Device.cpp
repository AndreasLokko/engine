//
// Created by lokko on 7.07.17.
//

#include <set>
#include <iostream>
#include "Device.hpp"
#include "ValidationLayers.hpp"
#include "../Common/Window.hpp"
#include "../Common/CommandPool.hpp"

Device::Device(const PhysicalDevice& physicalDevice)
: physicalDevice{physicalDevice.handle}
{
    std::vector<VkDeviceQueueCreateInfo> deviceQueueCreateInfos;
    const std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

    uint32_t i = 0;
    static float queuePriority = 1.0f;
    for (auto queueFamily : physicalDevice.queueFamilies) {
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueFamilyIndex = i++;
        deviceQueueCreateInfo.queueCount = queueFamily.properties.queueCount;
        deviceQueueCreateInfo.pQueuePriorities = &queuePriority;
        deviceQueueCreateInfos.push_back(deviceQueueCreateInfo);
    }

    static VkPhysicalDeviceFeatures deviceFeatures = {};

    VkDeviceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(deviceQueueCreateInfos.size());
    createInfo.pQueueCreateInfos = deviceQueueCreateInfos.data();
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = deviceExtensions.data();

    if (ValidationLayers::enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(ValidationLayers::validationLayers.size());
        createInfo.ppEnabledLayerNames = ValidationLayers::validationLayers.data();
    } else
        createInfo.enabledLayerCount = 0;

    if (vkCreateDevice(physicalDevice.handle, &createInfo, nullptr, &handle) != VK_SUCCESS)
        throw std::runtime_error("failed to create logical device!");

    commandPool = CommandPool(handle).handle;
}

Device::~Device() {
    vkDestroyCommandPool(handle, commandPool, nullptr);
    vkDestroyDevice(handle, nullptr);
}

void DeviceRequirements::addQueueRequirement(const DeviceRequirements::QueueRequirement& qRequirement)
{
    queueRequirements.push_back(qRequirement);
}

DeviceRequirements::DeviceRequirements(const VkInstance& instance)
        : instance{instance}{}