//
// Created by lokko on 4.07.17.
//

#ifndef ENGINE_VALIDATIONLAYERS_HPP
#define ENGINE_VALIDATIONLAYERS_HPP

#include "glfw3.hpp"
#include <vector>

class ValidationLayers
{
    const VkInstance& instance;
    static VkDebugReportCallbackEXT callback;
    static VkResult CreateDebugReportCallbackEXT(const VkInstance& instance,
                                                 const VkDebugReportCallbackCreateInfoEXT& pCreateInfo,
                                                 const VkAllocationCallbacks* pAllocator,
                                                 VkDebugReportCallbackEXT& pCallback);

    static VkBool32 debugCallback(VkDebugReportFlagsEXT flags,
                                  VkDebugReportObjectTypeEXT objType,
                                  uint64_t obj,
                                  size_t location,
                                  int32_t code,
                                  const char *layerPrefix,
                                  const char *msg,
                                  void *userData);
    public:
        void setupDebugCallback();
        void checkValidationLayerSupport();

    #ifdef NDEBUG
        static const bool enableValidationLayers = false;
    #else
        static const bool enableValidationLayers = true;
    #endif

    ~ValidationLayers();

    static std::vector<const char*> validationLayers;

    ValidationLayers(VkInstance &instance);
};


#endif //ENGINE_VALIDATIONLAYERS_HPP
