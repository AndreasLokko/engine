//
// Created by lokko on 8.07.17.
//

#ifndef ENGINE_PHYSICALDEVICE_HPP
#define ENGINE_PHYSICALDEVICE_HPP

#include "glfw3.hpp"
#include <vector>
#include <memory>

#define  collectPhysicalDevices(instance) \
_collectPhysicalDevices(instance, __LINE__, __FILE__)

class PhysicalDevice
{
    struct QueueFamily
    {
        VkQueueFamilyProperties properties;
        int presentable;
        QueueFamily(const VkQueueFamilyProperties& properties,
                    int presentable);
    };
    struct QueueFamilies : std::vector<QueueFamily>
    {
        QueueFamilies(VkPhysicalDevice handle,
                      VkInstance instance);
        QueueFamilies();
    };

public:

    VkPhysicalDevice handle;
    bool deviceReserved;
    bool deviceInUse;
    QueueFamilies queueFamilies;

    PhysicalDevice(VkPhysicalDevice handle,
                   VkInstance instance);
    PhysicalDevice();
};

void _collectPhysicalDevices(const VkInstance& instance,
                             int line,
                             const char* file);
PhysicalDevice reserveDevice();
PhysicalDevice useDevice();

#endif //ENGINE_PHYSICALDEVICE_HPP