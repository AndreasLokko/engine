//
// Created by lokko on 4.07.17.
//

#include <cstring>
#include <iostream>
#include <Application.hpp>
#include "ValidationLayers.hpp"

VkDebugReportCallbackEXT ValidationLayers::callback;

VKAPI_ATTR VkBool32 VKAPI_CALL ValidationLayers::debugCallback(VkDebugReportFlagsEXT flags,
                                                               VkDebugReportObjectTypeEXT objType,
                                                               uint64_t obj,
                                                               size_t location,
                                                               int32_t code,
                                                               const char* layerPrefix,
                                                               const char* msg,
                                                               void* userData)
{
    std::cerr << "validation layer: " << msg << std::endl;
    return VK_FALSE;
}

std::vector<const char*> ValidationLayers::validationLayers = {
        "VK_LAYER_LUNARG_standard_validation"
};

void ValidationLayers::setupDebugCallback() {
    if (!enableValidationLayers)
        return;
    VkDebugReportCallbackCreateInfoEXT createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createInfo.pfnCallback = debugCallback;
    if (CreateDebugReportCallbackEXT(instance, createInfo, nullptr, callback) != VK_SUCCESS)
        throw std::runtime_error("failed to set up debug callback!");
}

ValidationLayers::~ValidationLayers() {
    auto func = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(
            vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT"));
    if (func != nullptr)
        func(instance, callback, nullptr);
}

VkResult ValidationLayers::CreateDebugReportCallbackEXT(const VkInstance& instance, const VkDebugReportCallbackCreateInfoEXT& pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT& pCallback)
{
    auto func = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT"));
    if (func != nullptr)
        return func(instance, &pCreateInfo, pAllocator, &pCallback);
    else
        return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void ValidationLayers::checkValidationLayerSupport() {
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
    std::vector<VkLayerProperties> availableLayers(layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());
    if (enableValidationLayers)
        for (const char* layerName : validationLayers)
            [&]{for (const auto& layerProperties : availableLayers)
                    if (strcmp(layerName, layerProperties.layerName) == 0)
                        return;
                throw std::runtime_error("validation layers requested, but not available!");};
}

ValidationLayers::ValidationLayers(VkInstance& instance)
        : instance(instance)
{

}
