//
// Created by lokko on 26.09.17.
//

#include <cassert>
#include "ConsoleCommands.hpp"

std::map<std::string, ConsoleCommands::CLI_function> ConsoleCommands::commands;

//std::vector<ConsoleCommands::string> ConsoleCommands::parseArgs(ConsoleCommands::string args) {
//    std::vector<string> strings;
//    size_t last = 0, first = args.find_first_not_of(whitespace);
//    while (last != string::npos){
//        if(args.substr(first, 1) == "\"" ) {
//            last = args.find("\"", ++first);
//            while (last != string::npos) {
//                if (args.substr(last - 1, 1) != "\\") break;
//                args.erase(last - 1, 1);
//                last = args.find("\"", ++last);
//            }
//            if(last == string::npos) uts();
//        }
//        else {
//            last = args.find_first_of(whitespace, first);
//        }
//        strings.push_back(args.substr(first, last - first));
//        first = args.find_first_not_of(whitespace, last + 1);
//    }
//    return  strings;
//}

std::string ConsoleCommands::dm(const char *s) {
    return abi::__cxa_demangle(s, 0, 0, nullptr);
}

void ConsoleCommands::uts() {
    std::cout << "WARNING: Unterminated string processed in CLI" << std::endl;
}

void ConsoleCommands::fail() {
    throw std::invalid_argument("invalid argument to stream_function");
}

void ConsoleCommands::eof() {
    throw std::range_error("too many arguments");
}

void ConsoleCommands::parseCmd(std::istream&& cmd) {
    std::string s;
    cmd >> s;
    auto f = commands.find(s);
    if (f != commands.end()) {
        f->second(cmd);
    } else
        std::cout << "oh no" << std::endl;
}