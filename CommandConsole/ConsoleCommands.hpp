//
// Created by lokko on 26.09.17.
//

#ifndef ENGINE_CONSOLECOMMAND_HPP
#define ENGINE_CONSOLECOMMAND_HPP

#include <functional>
#include <map>
#include <iostream>
#include <tuple>
#include <cxxabi.h>
#include <typeindex>
#include <sstream>

//inline std::istream& operator>>(std::istream& is, std::string& str);

struct ConsoleCommands {

    static std::string dm(const char *s);
    static void uts();
    static void fail();
    static void eof();

    constexpr static std::ostream& out = std::cout;

    template<typename Func, typename Sig>
    struct CLI_function_t;

    template<typename Func, typename R, typename... Args>
    struct CLI_function_t<Func, R(*)(Args...)> {
        Func _f;
        CLI_function_t(const Func& f): _f(f) {}

//        friend std::istream& operator>>(std::istream& is, std::string& str);

        void operator()(std::istream& input) const{
            if constexpr (std::is_void<R>::value)
                _f([&](){Args x; input >> x; return x;}()...);
            else
                out << _f([&](){Args x; input >> x; return x;}()...);
        }

        template<typename T>
        static T get(std::istream& input){ T t; input >> t; return t;};
    };

    template<typename F>
    static CLI_function_t<F, F> CLI_function_c(const F& f) { return f; }

    typedef std::function<void(std::istream&)> CLI_function;

protected:
    static std::map<std::string, CLI_function> commands;

public:

    template<typename Fn>
    static void registerCommand(std::string&& s, const Fn& fn) {
        commands.emplace(s, CLI_function_c(fn));
    }

    static void parseCmd(std::istream&& cmd);
};

#endif //ENGINE_CONSOLECOMMAND_HPP
